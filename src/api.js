import axios from 'axios';
import cookie from 'js-cookie';
import { generateSlug } from './utils';

const baseUrl = 'http://localhost:4000';

export const getUserByEmail = async (email) => await axios.get(`${baseUrl}/users?email=${email}`);

export const loginUser = async (email, password) => {
  // check if we have a user with email
  const user = await getUserByEmail(email);

  // Log in user if we have a user
  if (user.data.length) {
    const loggedInUser = await axios.post(`${baseUrl}/login`, { email, password });
    if (loggedInUser) {
      const userData = {
        ...loggedInUser.data,
        email
      };
      cookie.set('user', JSON.stringify(userData));
      return loggedInUser.data;
    }
  }
  
  // otherwise register user
  const registeredUser = await axios.post(`${baseUrl}/register`, { email, password });
  if (registeredUser) {
    const userData = {
      ...registeredUser.data,
      email
    };
    cookie.set('user', JSON.stringify(userData));
    return registeredUser.data;
  }
}

export const logoutUser = async () => {
  cookie.remove('user');
  window.location.reload();
}

export const createPost = async (title, content) => {
  const post = await axios.post(`${baseUrl}/posts`, {
    date_created: new Date().toLocaleDateString(),
    title,
    content,
    slug: generateSlug(title)
  });
  
  if (post) {
    return post.data;
  }
}

export const getPostBySlug = async (slug) => {
  const post = await axios.get(`${baseUrl}/posts?slug=${slug}`);
  if (post) {
    return post.data;
  }
}

export const getAllPosts = async () => {
  const posts = await axios.get(`${baseUrl}/posts`);
  if (posts.data.length) {
    return posts.data;
  }
}
