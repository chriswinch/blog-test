import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Store from '../context';

const AuthRoute = lazy(() => import('../AuthRoute'));
const Home = lazy(() => import('../Home'));
const Post = lazy(() => import('../Post'));
const Admin = lazy(() => import('../Admin'));
const CreatePost = lazy(() => import('../CreatePost'));
const Profile = lazy(() => import('../Profile'));
const Login = lazy(() => import('../Login'));
const Header = lazy(() => import('../Header'));

function App() {
  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <Store>
        <div className="App">
          <Router>
            <Header />
            <Switch>
              <Route exact path="/"><Home /></Route>
              <Route path="/post/:id"><Post /></Route>
              <AuthRoute path="/admin/create"><CreatePost /></AuthRoute>
              <AuthRoute path="/admin/profile"><Profile /></AuthRoute>
              <AuthRoute path="/admin"><Admin /></AuthRoute>
              <Route path="/login"><Login /></Route>
              <Route path="*"><h1>404 not found</h1></Route>
            </Switch>
          </Router>
        </div>
      </Store>
    </Suspense>
  );
}

export default App;
