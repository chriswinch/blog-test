import React from 'react';
import { Link } from 'react-router-dom';

const Admin = () => {
  return (
    <div>
      <h1>Admin</h1>
      <p><Link to="/admin/create">Create a new blog post</Link></p>
      <p><Link to="/admin/profile">Your Profile</Link></p>
    </div>
  )
};

export default Admin;
