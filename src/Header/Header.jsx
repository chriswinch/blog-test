import React, { useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { logoutUser } from '../api';
import { useAppContext, constants } from '../context';
import cookie from 'js-cookie';

const Header = () => {
  const history = useHistory();
  const [{ currentUser }, dispatch] = useAppContext();
  const user = cookie.get('user');

  useEffect(() => {
    if (user) {
      dispatch({ type: constants.SET_CURRENT_USER, currentUser: JSON.parse(user) })
    } else {
      dispatch({ type: constants.SET_CURRENT_USER, currentUser: null });
    }
  }, [dispatch, user])

  return (
    <div>
      <Link to="/"><h1>Blog</h1></Link>

      <ul>
        <li><Link to="/">All Posts</Link></li>
        <li><Link to="/admin">Admin</Link></li>
        <li><Link to="/admin/create">Create Post</Link></li>
        <li><Link to="/admin/profile">Profile</Link></li>
      </ul>

      {!currentUser && history.location.pathname !== "/login" &&
        <Link to="/login">Login / Register</Link> 
      }
      
      {currentUser && <button onClick={logoutUser}>Logout</button>}
    </div>
  )
}

export default Header;
