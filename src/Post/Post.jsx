import React, { useEffect, useState } from 'react';
import { getPostBySlug } from '../api';
import { useHistory } from 'react-router-dom';

const Post = () => {
  const history = useHistory();
  const [post, setPost] = useState(null);
  const slug = history.location.pathname.split('/')[2];
  
  useEffect(() => {
    (async () => {
      const fetchedPost = await getPostBySlug(slug);
      const { title, date_created, content } = fetchedPost && fetchedPost[0];
      setPost({ title, date_created, content })
    })();
  }, [slug])
  
  if (post) {
    return (
      <div>
        <h1>{post.title}</h1>
        <span>{post.date_created}</span>
        <p>{post.content}</p>
      </div>
    )
  }

  return null;
};

export default Post;
