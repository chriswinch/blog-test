// Very simple slug generator, could be expanded upon to handle special characters
export const generateSlug = (title) => title.toLowerCase().replace(/ /g, '-');
