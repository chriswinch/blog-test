import React from 'react';
import { useAppContext } from '../context';

const Profile = () => {
  const [{ currentUser }] = useAppContext();

  return (
    <div>
      <p>Nothing much to see here.</p>
      <p>Your email is {currentUser && currentUser.email}</p>
    </div>
  )
};

export default Profile;
