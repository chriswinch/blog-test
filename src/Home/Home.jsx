import React, { useEffect } from 'react';
import { useAppContext, constants } from '../context';
import { getAllPosts } from '../api';
import { Link } from 'react-router-dom';

const Home = () => {
  const [{ posts }, dispatch] = useAppContext();
  
  useEffect(() => {
    (async () => {
      const posts = await getAllPosts();
      if (posts) {
        dispatch({ type: constants.GET_ALL_POSTS, posts })
      }
    })()
  }, [dispatch])

  return (
    <div>
      <h1>Home</h1>
      {posts.length ? (
        posts.map((post) => {
          return (
            <Link
              key={post.id}
              to={`/post/${post.slug}`}
            >
              <h3>{post.title}</h3>
              <p>{post.date_created}</p>
              <hr/>
            </Link>
          )
        })
      ) : (
        <div>
          <h3>No Posts</h3>
          <Link to="/admin/create">Create a new post</Link>
        </div>
      )}
    </div>
  )
};

export default Home;
