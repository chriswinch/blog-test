import React from 'react';
import { createPost } from '../api';
import { useHistory } from 'react-router-dom';

const CreatePost = () => {
  const history = useHistory();
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    const { title, content } = e.target;
    const post = await createPost(title.value, content.value);
    history.push(`/post/${post.slug}`);
  }

  return (
    <div>
      <h1>Create Post</h1>
      <form onSubmit={handleSubmit}>
        <p><input type="text" placeholder="title" name="title" /></p>
        <p><textarea placeholder="content" name="content"></textarea></p>
        <p><button type="submit">Create Post</button></p>
      </form>
    </div>
  )
};

export default CreatePost;
