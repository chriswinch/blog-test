import React, { createContext, useContext, useReducer } from 'react';

export const AppContext = createContext();

export const useAppContext = () => useContext(AppContext);

export const constants = {
  ADD_POST: 'ADD_POST',
  SET_CURRENT_USER: 'SET_CURRENT_USER',
  GET_ALL_POSTS: 'SET_ALL_POSTS'
}

export const initialState = {
  posts: [],
  currentUser: null,
}

const reducer = (state, action) => {
  switch (action.type) {
    case constants.ADD_POST: {
      return;
    }
    case constants.SET_CURRENT_USER: {
      return { ...state, currentUser: action.currentUser };
    }
    case constants.GET_ALL_POSTS: {
      return { ...state, posts: action.posts };
    }
    default:
      return state;
  }
}

const Store = ({ children }) => {
  return (
    <AppContext.Provider value={useReducer(reducer, initialState)}>
      {children}
    </AppContext.Provider>
  )
};

export default Store;
