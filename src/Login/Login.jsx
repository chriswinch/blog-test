import React, { useEffect } from 'react';
import { loginUser } from '../api';
import { useHistory } from 'react-router-dom';
import { useAppContext, constants } from '../context';

const Login = () => {
  const [{ currentUser }, dispatch] = useAppContext();
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { email, password } = e.target;
    const user = await loginUser(email.value, password.value);
    if (user) {
      dispatch({ type: constants.SET_CURRENT_USER, currentUser: user });
      history.push('/admin');
    }
  }

  useEffect(() => {
    if (currentUser) {
      history.push('/admin');
    }
  }, [currentUser, history])

  return (
    <form onSubmit={handleSubmit} style={{ marginTop: 20 }}>
      <input type="email" placeholder="email" name="email" />
      <input type="password" placeholder="password" name="password" />
      <button type="submit">Register / Login</button>
    </form>
  )
};

export default Login;
