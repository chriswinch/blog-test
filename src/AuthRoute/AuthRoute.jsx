import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useAppContext } from '../context';
import cookie from 'js-cookie';

const AuthRoute = ({ to, children }) => {
  const [{ currentUser }] = useAppContext();
  const user = cookie.get('user'); 

  if (!currentUser && !user) {
    return <Redirect to="/login" />;
  }
  
  return (
    <Route to={to}>{children}</Route>
  )
}

export default AuthRoute;
